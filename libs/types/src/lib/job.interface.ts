export interface IJob {
  jobTitle: string;

  description: string;

  location: Location;
}

export enum Location {
  BERLIN = 'Berlin',
  HAMBURG = 'Hamburg',
  REMOTE = 'Remote',
}
