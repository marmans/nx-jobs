import { Property, PrimaryKey, Entity } from '@mikro-orm/core';
import { v4 } from 'uuid';
import { IBaseEntity } from './base.interface';

@Entity({
  abstract: true,
})
export abstract class BaseEntity implements IBaseEntity {
  @PrimaryKey()
  uuid: string = v4();

  @Property()
  createdAt: Date = new Date();

  @Property({ onUpdate: () => new Date() })
  updatedAt: Date = new Date();
}
