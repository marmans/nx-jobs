export interface IBaseEntity {
  uuid: string;

  createdAt: Date;

  updatedAt: Date;
}
