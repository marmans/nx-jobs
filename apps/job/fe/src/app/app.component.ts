import { Component } from '@angular/core';

@Component({
  selector: 'finlink-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'job-fe';
}
