import { Migration } from '@mikro-orm/migrations';

export class Migration20211031151805 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "job" ("uuid" varchar(255) not null, "created_at" timestamptz(0) not null, "updated_at" timestamptz(0) not null, "job_title" varchar(255) not null, "description" varchar(255) not null, "location" text check ("location" in (\'Berlin\', \'Hamburg\', \'Remote\')) not null);');
    this.addSql('alter table "job" add constraint "job_pkey" primary key ("uuid");');
  }

}
