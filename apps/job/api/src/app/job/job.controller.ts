import {
  Controller,
  Get,
  Query,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ParseUUIDPipe,
  HttpStatus,
  UsePipes,
} from '@nestjs/common';
import { JobService } from './job.service';
import { IJob, PaginationQueryDto } from '@finlink/types';
import { ValidationPipe } from '@finlink/shared';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';

@Controller('jobs')
export class JobController {
  constructor(private readonly jobService: JobService) {}

  @Get()
  async findAll(
    @Query() paginationQuery: PaginationQueryDto
  ): Promise<[IJob[], number]> {
    return this.jobService.findAllAndCount(paginationQuery);
  }

  @Get(':id')
  async findOne(
    @Param(
      'id',
      new ParseUUIDPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })
    )
    id: string
  ): Promise<IJob> {
    return this.jobService.findOne(id);
  }

  @Post()
  @UsePipes(new ValidationPipe())
  create(@Body() createJobDto: CreateJobDto): Promise<IJob> {
    return this.jobService.create(createJobDto);
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  update(
    @Param('id') id: string,
    @Body() updateJobDto: UpdateJobDto
  ): Promise<IJob> {
    return this.jobService.update(id, updateJobDto);
  }

  @Delete(':id')
  delete(@Param('id', ParseUUIDPipe) id: string): Promise<void> {
    return this.jobService.delete(id);
  }
}
