import {
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { assign, EntityRepository } from '@mikro-orm/core/';
import { InjectRepository } from '@mikro-orm/nestjs';
import { IJob, PaginationQueryDto } from '@finlink/types';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { Job } from './entities/job.entity';

@Injectable()
export class JobService {
  logger = new Logger(JobService.name);

  constructor(
    @InjectRepository(Job) private readonly jobRepository: EntityRepository<Job>
  ) {}

  async findAllAndCount(
    paginationQuery: PaginationQueryDto
  ): Promise<[IJob[], number]> {
    return this.jobRepository.findAndCount(
      {},
      { limit: paginationQuery.limit, offset: paginationQuery.page }
    );
  }

  async findOne(id: string): Promise<Job> {
    const job = await this.jobRepository.findOne(id);
    if (!job) {
      throw new NotFoundException(`Job #${id} not found`);
    }
    return job;
  }

  async create(createJobDto: CreateJobDto): Promise<IJob> {
    const exists = await this.jobRepository.count({
      jobTitle: createJobDto.jobTitle,
    });

    if (exists > 0) {
      throw new HttpException(
        {
          message: 'Input data validation failed',
          errors: { jobTitle: 'Title must be unique.' },
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const job = this.jobRepository.create({
      ...createJobDto,
    });

    await this.jobRepository.persistAndFlush(job);
    return job;
  }

  async update(id: string, updateJobDto: UpdateJobDto): Promise<IJob> {
    const job = await this.jobRepository.findOne(id);
    if (!job) {
      throw new NotFoundException(`Job #${id} not found`);
    }

    if (updateJobDto.jobTitle) {
      const exists = await this.jobRepository.count({
        $and: [{ uuid: { $ne: id } }, { jobTitle: updateJobDto.jobTitle }],
      });

      if (exists > 0) {
        throw new HttpException(
          {
            message: 'Input data validation failed',
            errors: { jobTitle: 'Title must be unique.' },
          },
          HttpStatus.BAD_REQUEST
        );
      }
    }

    assign(job, updateJobDto);
    await this.jobRepository.flush();

    return job;
  }

  async delete(id: string): Promise<void> {
    const job = await this.findOne(id);
    return this.jobRepository.removeAndFlush(job);
  }
}
