import { IsEnum, IsString } from 'class-validator';
import { Location } from '@finlink/types';

export class CreateJobDto {
  @IsString()
  jobTitle: string;

  @IsString()
  description: string;

  @IsEnum(Location)
  location: Location;
}
