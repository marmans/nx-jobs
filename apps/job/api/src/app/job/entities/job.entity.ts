import { Entity, Enum, Property } from '@mikro-orm/core';
import { BaseEntity, IJob, Location } from '@finlink/types';

@Entity()
export class Job extends BaseEntity implements IJob {
  @Property()
  jobTitle!: string;

  @Property()
  description!: string;

  @Enum(() => Location)
  location!: Location;
}
