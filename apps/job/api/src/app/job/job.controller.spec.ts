import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { v4 } from 'uuid';
import { IJob, Location, PaginationQueryDto } from '@finlink/types';
import { Job } from './entities/job.entity';
import { JobController } from './job.controller';
import { JobService } from './job.service';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import mikroOrmConfig from '../../../mikro-orm.config';
import { INestApplication } from '@nestjs/common';

describe('jobController', () => {
  let app: INestApplication;
  let controller: JobController;
  let service: JobService;

  const job1: Job = {
    uuid: v4(),
    createdAt: new Date(),
    updatedAt: new Date(),
    jobTitle: 'jobTitleBerlin',
    description: 'descriptionBerlin',
    location: Location.BERLIN,
  };
  const job2: Job = {
    uuid: v4(),
    createdAt: new Date(),
    updatedAt: new Date(),
    jobTitle: 'jobTitleHamburg',
    description: 'descriptionHamburg',
    location: Location.HAMBURG,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MikroOrmModule.forRoot(mikroOrmConfig),
        MikroOrmModule.forFeature([Job]),
      ],
      controllers: [JobController],
      providers: [JobService],
    }).compile();

    service = module.get<JobService>(JobService);
    controller = module.get<JobController>(JobController);
    app = module.createNestApplication();
    await app.init();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('returns some jobs', async () => {
      const mockQuery: PaginationQueryDto = {
        limit: 2,
        page: 0,
      };
      const mockApiResponse: [IJob[], number] = [[job1, job2], 2];

      jest.spyOn(service, 'findAllAndCount').mockResolvedValue(mockApiResponse);

      const actual = controller.findAll(mockQuery);

      expect(service.findAllAndCount).toHaveBeenCalledTimes(1);
      expect(service.findAllAndCount).toHaveBeenCalledWith(mockQuery);
      expect(actual).resolves.toEqual(mockApiResponse);
    });

    it('returns 200 and empty array when no jobs', async () => {
      const mockQuery: PaginationQueryDto = {
        limit: 0,
        page: 0,
      };
      const mockApiResponse: [IJob[], number] = [[], 2];

      jest.spyOn(service, 'findAllAndCount').mockResolvedValue(mockApiResponse);

      return request(app.getHttpServer())
        .get('/jobs')
        .send(mockQuery)
        .expect(200, mockApiResponse);
    });
  });
});
