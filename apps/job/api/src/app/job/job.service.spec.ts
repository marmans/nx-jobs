import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Test, TestingModule } from '@nestjs/testing';
import mikroOrmConfig from '../../../mikro-orm.config';
import { Job } from './entities/job.entity';
import { JobService } from './job.service';

describe('jobService', () => {
  let service: JobService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MikroOrmModule.forRoot(mikroOrmConfig),
        MikroOrmModule.forFeature([Job]),
      ],
      providers: [JobService],
    }).compile();

    service = module.get<JobService>(JobService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('update', () => {
    it.todo('can not update, if another job has the same jobTitle');

    it.todo('can update, if only the job itself has this jobTitle');
  });
});
