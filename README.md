# jobs

1. Install dependencies via `npm install`

2. Run `docker-compose up -d` to start the postgres database and adminer dashboard

3. Run `npm i -g @mikro-orm/postgresql`

4. Run `npm i -g newman`

5. Create database schema via `mikro-orm migration:up`. This will also create the database if it does not exist

6. Run using `npm start`

7. Run using `nx lint`

8. Run using `npm test`

9. API will be available at http://localhost:3333/api/jobs

10. Run `npm run job:e2e`, you can add `-r htmlextra` to generate html report

11. Use `jobs.postman_collection.json` from `apps/job/api-e2e` in Postman